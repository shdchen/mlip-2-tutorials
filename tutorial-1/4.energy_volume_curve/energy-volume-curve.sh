#!/bin/bash

export OMP_NUM_THREADS=1

../mlp calc-efs pot.mtp deformed.cfg deformed_efs.cfg
python cfg-to-energy-volume.py deformed_efs.cfg E_V.txt
