#!/bin/bash

export OMP_NUM_THREADS=1

../mlp train init.mtp train.cfg --trained-pot-name=pot.mtp --valid-cfgs=test.cfg
