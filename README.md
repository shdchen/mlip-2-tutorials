# MLIP-2 Tutorials

Following the link https://gitlab.com/ashapeev/mlip-2-tutorials/-/wikis/home one can find 
a tutorial about installation of `MLIP` software (Tutorial 0), and a tutorial with an example
of `MLIP` usage (Tutorial 1). 

The folders `tutorial-1/` and `tutorial-1-answers/` are 
input, output, and intermediate files, and different stages of the calculation of Mo elastic 
constants. See the description of the files inside these folders (the README files).
